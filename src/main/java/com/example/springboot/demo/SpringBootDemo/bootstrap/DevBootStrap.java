package com.example.springboot.demo.SpringBootDemo.bootstrap;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.example.springboot.demo.SpringBootDemo.Author;
import com.example.springboot.demo.SpringBootDemo.Book;
import com.example.springboot.demo.SpringBootDemo.Publisher;
import com.example.springboot.demo.SpringBootDemo.repositories.AuthorRepository;
import com.example.springboot.demo.SpringBootDemo.repositories.BookRepository;
import com.example.springboot.demo.SpringBootDemo.repositories.PublisherRepository;

@Component
public class DevBootStrap implements ApplicationListener<ContextRefreshedEvent> {

	private AuthorRepository authorRepository;
	private BookRepository bookRepository;
	private PublisherRepository publisherRepository;
	
	
	public DevBootStrap() {
		
	}
	
	public DevBootStrap(AuthorRepository authorRepository, BookRepository bookRepository,
			PublisherRepository publisherRepository) {
		super();
		this.authorRepository = authorRepository;
		this.bookRepository = bookRepository;
		this.publisherRepository = publisherRepository;
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		initData();
	}
	
	private void initData() {
		
		Publisher publisher = new Publisher("Somerville's Publishing","Delhi");

		//Eric
		Author eric = new Author("Eric","Evans");
		Book ddd = new Book("Domain Driven Design","1234",publisher );
		eric.getBooks().add(ddd);
		ddd.getAuthors().add(eric);
		
		publisherRepository.save(publisher);
		authorRepository.save(eric);
		bookRepository.save(ddd);
		
		//Rod
		Author rod = new Author("Rod", "Johnson");
		Book noEJB = new Book("J2EE Devop","2345", publisher);
		rod.getBooks().add(noEJB);
		noEJB.getAuthors().add(rod);
		
		authorRepository.save(rod);
		bookRepository.save(noEJB);
		
	}
	

}
