package com.example.springboot.demo.SpringBootDemo.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.springboot.demo.SpringBootDemo.Publisher;

public interface PublisherRepository extends CrudRepository<Publisher, Long> {

}
